# Football Prediction

Build a model to predict the outcome of a football match, given data for the past 9 years
Based on the dataset provided, your goal should be to come up with an optimal solution to predict if a Home Team would win or lose or draw a game (column name ​